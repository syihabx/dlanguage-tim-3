export const rupiah = (number) => {
  const fixedNumber = Math.round(number);
  let price = fixedNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  let data = "IDR " + price;
  return data;
};
