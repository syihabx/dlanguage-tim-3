import { Box, Typography } from "@mui/material";
import React from "react";
import { hero } from "../../assets/img";

export const LandingPageHero = () => {
  return (
    <Box
      sx={{
        bgcolor: "#EA9E1F",
        minHeight: "403px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        my: { xs: 5, md: 10 },
        p: { xs: 5, md: 0 },
        flexDirection: { xs: "column", md: "row" },
      }}
    >
      <Box maxWidth="md">
        <Typography variant="h4" color="#FFFFFF" marginBottom={5}>
          Gets your best benefit
        </Typography>
        <Typography
          color="#FFFFFF"
          style={{ textAlign: "justify", fontSize: "16px" }}
        >
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
          ab illo inventore veritatis et quasi architecto beatae vitae dicta
          sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
          aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
          qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
          dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed
          quia non numquam eius modi tempora incidunt ut labore et dolore magnam
          aliquam quaerat voluptatem.
        </Typography>
      </Box>
      <Box maxWidth="xs" sx={{ mt: { xs: 5, md: 0 } }}>
        <img src={hero} alt="Hero" />
      </Box>
    </Box>
  );
};
