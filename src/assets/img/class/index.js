import arabic from './arabic.png';
import basicenglish from './basicenglish.png'
import expertenglish from './expertenglish.png'
import germany from './germany.png'
import indonesia from './indonesia.png'
import mandarin from './mandarin.png'

export { arabic, basicenglish, expertenglish, germany, indonesia, mandarin };